#include "Stack.h"

void StackInit(Stack* ps)
{
	assert(ps);
	ps->top = 0;
	ps->capacity = 0;
	ps->data = NULL;
}

void StackPrint(const Stack* ps)
{
	assert(ps);
	int a = 0;
	while (a < ps->top)
	{
		printf("%d ", ps->data[a]);
		a++;
	}
	printf("\n");

}
void StackPush(Stack* ps, DataType x)
{
	assert(ps);
	if (ps->data == NULL)
	{
		ps->data = malloc(2 * sizeof(DataType));
		ps->capacity = 2;
	}
	else if(ps->capacity==ps->top)
	{
		DataType* ins = realloc(ps->data, 2 * sizeof(DataType) * ps->capacity);
		ps->capacity *= 2;
	}
	ps->data[ps->top] = x;
	ps->top++;
}

void StackPop(Stack* ps)
{
	assert(ps);
	ps->top--;
}

bool StackEmpty(Stack* ps)
{
	assert(ps);
	if (ps->data == NULL)
	{
		return false;
	}
	else
		return true;
}

int StackCheck(Stack* ps)
{
	return ps->top;
}

void StackDestroy(Stack* ps)
{
	assert(ps);
	free(ps->data);
	ps->data = NULL;
	ps->data = ps->top = 0;
}