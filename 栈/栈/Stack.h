#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef int DataType;
struct Stack
{
	DataType* data;
	int top;
	int capacity;
};

typedef struct Stack Stack;

//初始化栈
void StackInit(Stack* ps);

//栈进
void StackPush(Stack* ps,DataType x);

//栈出
void StackPop(Stack* ps);

//查看数据
int StackCheck(Stack* ps);

//判断是否为空
bool StackEmpty(Stack* ps);

//打印栈
void StackPrint(const Stack* ps);

//销毁栈
void StackDestroy(Stack* ps);