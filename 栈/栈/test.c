#include "Stack.h"

void test()
{
	Stack sk;
	StackInit(&sk);
	StackPush(&sk, 7);
	StackPush(&sk, 6);
	StackPush(&sk, 5);
	StackPush(&sk, 4);

	StackPrint(&sk);

	StackPop(&sk);
	StackPrint(&sk);

	StackDestroy(&sk);
}


int main()
{
	test();
	return 0;
}