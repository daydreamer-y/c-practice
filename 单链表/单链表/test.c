#include "SList.h"


void test1()
{
	SListNode* phead = NULL;
	SListPushFront(&phead, 6);
	SListPushBack(&phead, 1);
	SListInsert(&phead, 3, 1);
	SListEraseAfter(&phead, 1);
	SListPushBack(&phead, 2);
	SListPushBack(&phead, 3);
	//SListPushFront(&phead, 6);
	SListprint(phead);
	SListPushBack(&phead, 4);
	SListPushBack(&phead, 5);
	SListprint(phead);
	SListPopBack(&phead);
	SListPopBack(&phead);
	SListprint(phead);
}

int main()
{
	test1();
	return 0;
}

