#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef int SLTDataType;

struct SListNode
{
	SLTDataType data;
	struct SListNode* next;
};

typedef struct SListNode SListNode;

//打印链表
void SListprint(SListNode* phead);

//尾插
void SListPushBack(SListNode** pphead,SLTDataType x);

//尾删
void SListPopBack(SListNode** pphead);

//头插
void SListPushFront(SListNode** pphead, SLTDataType x);

//头删
void SListPopFront(SListNode** pphead);

//寻找一个数的位置
SListNode* SListFind(SListNode* phead, SLTDataType x);

//链表在x位置后插入
void SListInsert(SListNode** pphead, SLTDataType insert, SLTDataType x);

//消除指定元素后面的
void SListEraseAfter(SListNode** pphead, SLTDataType x);
