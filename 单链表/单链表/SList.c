#include "SList.h"

void SListPushBack(SListNode** pphead,SLTDataType x)
{
	if (*pphead == NULL)
	{
		*pphead = (SListNode*)malloc(sizeof(SListNode));
		if (*pphead == NULL)
		{
			printf("β��ʧ��\n");
			return;
		}
		(*pphead)->data = x;
		(*pphead)->next = NULL;
		return;
	}
	else
	{
		SListNode* cur = *pphead;
		while (cur->next)
		{
			cur = cur->next;
		}
		cur->next = (SListNode*)malloc(sizeof(SListNode));
		cur->next->next = NULL;
		cur->next->data = x;
	}
}

void SListprint(SListNode* phead)
{
	SListNode* cur = phead;
	while (cur)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

void SListPopBack(SListNode** pphead)
{
	if ((*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
		return;
	}
	else if (*pphead == NULL)
	{
		return;
	}
	else
	{
		SListNode* cur = *pphead;
		SListNode* pre = NULL;
		while (cur->next)
		{
			pre = cur;
			cur = cur->next;
		}
		free(cur);
		pre->next = NULL;
	}
}

void SListPushFront(SListNode** pphead, SLTDataType x)
{
	if (*pphead == NULL)
	{
		SListPushBack(pphead, x);
		return;
	}
	else
	{
		SListNode* born = (SListNode*)malloc(sizeof(SListNode));
		if (born == NULL)
		{
			printf("ͷ��ʧ��\n");
			return;
		}
		SListNode* tem = born;
		born->data = x;
		born->next = *pphead;
		*pphead = tem;
	}
}

void SListPopFront(SListNode** pphead)
{
	SListNode* tem = (*pphead)->next;
	free(*pphead);
	*pphead = tem;
}

SListNode* SListFind(SListNode* phead, SLTDataType x)
{
	while (phead)
	{
		if (phead->data == x)
			return phead;
		phead = phead->next;
	}
	exit(-1);
}

// ˳�����posλ�ú����x
void SListInsert(SListNode** pphead, SLTDataType insert, SLTDataType x)
{
	SListNode* pos = SListFind(*pphead, x);
	if (pos == NULL)
	{
		printf("δ�ҵ�Ŀ��\n");
		exit(-1);
	}
	SListNode* behind = pos->next;
	pos->next = (SListNode*)malloc(sizeof(SListNode));
	if (!pos)
	{
		pos->next = behind;
		printf("����ʧ��");
		exit(-1);
	}
	pos->next->data = insert;
	pos->next->next = behind;
}

void SListEraseAfter(SListNode** pphead, SLTDataType x)
{
	SListNode* pos = SListFind(*pphead, x);
	if (pos == NULL)
	{
		printf("δ�ҵ�Ŀ��\n");
		exit(-1);
	}
	SListNode* target = pos->next;
	pos->next = target->next;
	free(target);
	target = NULL;
}
