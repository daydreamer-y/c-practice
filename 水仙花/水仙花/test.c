#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//描述：求出0~100000之间所有“水仙花数”并输出
//“水仙花数”是指一个n位数，其各位数字的n次方之和恰好等于该数本身
//如：153 = 1^3 + 5^3 + 3^3


int my_pow(int x, int n)
{
	int i = 0;
	int y = 1;
	for (i = 0; i < n; i++)
	{
		y *= x;
	}
	return y;
}
int main()
{
	int i = 1;
	for (i = 1; i <= 100000; i++)
	{
		//1.判断位数
		//2.求出各位数平方
		//3.判断是否等于其本身
		int n = 1;
		int temp = i;
		while (temp / 10)
		{
			n++;
			temp = temp / 10;
		}
		int sum = 0;
		temp = i;
		while (temp)
		{
			sum += my_pow(temp % 10, n);
			temp /= 10;
		}
		if (sum == i)
		{
			printf("%d\n", i);
		}
	}
	return 0;
}