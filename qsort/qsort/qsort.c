#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
//数组nums包含从0到n的所有整数，但其中缺了一个。请编写代码找出
//个缺失的整数。你有办法在0(n)时间内完成吗 ?
//注意 : 本题相对书上原题稍作改动
//示例1 :
//输入: [3, 0, 1]
//输出 : 2

//int main()
//{
//
//	return 0;
//}

////qsort实现冒泡排序
////先复习一下冒泡排序
void print(int ar[], int n)
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		printf("%d ", ar[i]);
	}
	printf("\n");
}
//void sort(int ar[], int n)
//{
//	int i = 0;
//	for (i = 0; i < n - 1; i++)
//	{
//		int j = 0;
//		for (j = i; j < n; j++)
//		{
//			if (ar[i] > ar[j + 1])
//			{
//				int tem = ar[i];
//				ar[i] = ar[j + 1];
//				ar[j + 1] = tem;
//			}
//		}
//	}
//}
//int main()
//{
//	int b[10] = { 2,4,6,5,1,3,8,7,9,0 };
//	int sz = sizeof b / sizeof b[0];
//	print(b, sz);
//	sort(b, sz);
//	print(b, sz);
//	return 0;
//}


////qsort实现冒泡排序
////先复习一下冒泡排序
//void print(int ar[], int n)
//{
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		printf("%d ", ar[i]);
//	}
//	printf("\n");
//}
//void bubble_sort(int ar[], int n)
//{
//	int i = 0;
//	for (i = 0; i < n-1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < n - 1 -i ; j++)
//		{
//			if (ar[j] > ar[j + 1])
//			{
//				int tem = ar[j];
//				ar[j] = ar[j + 1];
//				ar[j + 1] = tem;
//			}
//		}
//	}
//}
//int main()
//{
//	int b[10] = { 2,5,4,3,1,0,9,8,6,7 };
//	int sz = sizeof b / sizeof b[0];
//	print(b, sz);
//	bubble_sort(b, sz);
//	print(b, sz);
//	return 0;
//}


//void qsort(void* base,   //base中存放的是待排序数据中第一个对象的地址
//	size_t num,//排序数据元素的个数
//	size_t size, //排序数据中一个元素的大小，单位是字节
//	int (*cmp)(const void*, const void*)//比较待排序数据中两个元素
//)


//int cmp_int(const void* e1, const void* e2)
//{
//	return *(int*)e1 - *(int*)e2;
//}
//int main()
//{
//	int arr[10] = { 3,2,1,5,4,7,6,0,9,8 };
//	int sz = sizeof arr / sizeof arr[0];
//	print(arr, sz);
//	qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	print(arr, sz);
//	return 0;
//}


//在结构体中使用qsort
//#include <string.h>
//struct peo
//{
//	char name[20];
//	int age;
//};
//int sort_age(const void* e1, const void* e2)
//{
//	return ((struct peo*)e1)->age - ((struct peo*)e2)->age;
//}
////int sort_name(const void* e1, const void* e2)
////{
////	return strcmp(((struct peo*)e1)->name, ((struct peo*)e2)->name);
////}
//int main()
//{
//	struct peo arr[3] = { {"zhangsan",25},{"lisi",18},{"wanger",21} };
//	int sz = sizeof arr / sizeof arr[0];
//	qsort(arr, sz, sizeof(arr[0]), sort_age);
//	//qsort(arr, sz, sizeof(arr[0]), sort_name);
//	return 0;
//}







//模拟实现qsort的通用算法，不需要自己设计比较函数   bubble_qsort
//记得按时复习
void swap(char* e1, char* e2, int n)
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		char tem = *(e1 + i);
		*(e1 + i) = *(e2 + i);
		*(e2 + i) = tem;
	}
}
int cmp(const void* e1, const void* e2)
{
	return *((int*)e1) - *((int*)e2);
}
void bubble_qsort(int* base, int num, int size, int (*cmp)(const void*, const void*))
{
	int i = 0;
	for (i = 0; i < num; i++)
	{
		int j = 0;
		for (j = 0; j < num - i - 1; j++)
		{
			if (cmp((char*)base+j*size,(char*)base+(j+1)*size) > 0)
			{
				swap((char*)base + j * size, (char*)base + (j + 1) * size, size);
			}
		}
	}
}
 
int main()
{
	int arr[10] = { 3,2,1,5,4,7,6,0,9,8 };
	int sz = sizeof arr / sizeof arr[0];
	print(arr, sz);
	bubble_qsort(arr, sz, sizeof(arr[0]), cmp);
	print(arr, sz);
	return 0;
}
