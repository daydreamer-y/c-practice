#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>

#define MAX_name 10
#define MAX_sex 10
#define MAX_tele 12
#define MAX_addr 20
#define MAX 1000
#define DEFAULT_SZ 3
#define inc_sz 2

//联系人基本信息
typedef struct peoino
{
	char name[MAX_name];
	char sex[MAX_sex];
	int age;
	char tele[MAX_tele];
	char addr[MAX_addr];
}peoino;

//通讯录
//typedef struct Contact
//{
//	peoino data[MAX];
//	int sz;
//}Contact;

//动态通讯录
typedef struct Contact
{
	peoino* data;
	int sz;
	int capacity;
}Contact;

//初始化通讯录
void Initcontact(Contact* pc);

//增加联系人
void Addcontact(Contact* pc);

//打印电话簿
void Printcontact(const Contact* pc);

//删除联系人
void Delcontact(Contact* pc);

//查找联系人
void Searchcontact(const Contact* pc);

//修改联系人
void Modifycontact(Contact* pc);

//按照名字排序所有人
void Sortcontact(const Contact* pc);

//退出通讯录并释放内存
void Exitcon(Contact* pc);

//将通讯录保存在文件中
void savecontact(Contact* pc);

//判断是否增容
void checkcapacity(Contact* pc);

//对通讯录进行排序根据名字
void sort_name(Contact* pc);