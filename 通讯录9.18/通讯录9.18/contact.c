#include "contact.h"
//初始化
//void Initcontact(Contact* pc)
//{
//	pc->sz = 0;
//	memset(pc->data, 0, sizeof(pc->data));
//}
//动态初始化
void Initcontact(Contact* pc)
{
	pc->data = (peoino*)malloc(DEFAULT_SZ * sizeof(peoino));
	if (pc->data == NULL)
	{
		perror("Initcontact");
	}
	pc->sz = 0;
	pc->capacity = DEFAULT_SZ;
	//读取文件信息
	FILE* pf = fopen("contact.dat", "r");
	if (NULL == pf)
	{
		perror("fopen");
		return;
	}
	peoino tem = { 0 };
	while (fread(&tem, sizeof(peoino), 1, pf))
	{
		//判断是否要增容
		checkcapacity(pc);
		pc->data[pc->sz] = tem;
		pc->sz++;
	}
	//关闭文件
	fclose(pf);
	pf = NULL;
}

//退出并销毁
void Exitcon(Contact* pc)
{
	free(pc->data);
	pc->data = NULL;
	printf("退出通讯录\n");
}

//增加联系人
void Addcontact(Contact* pc)
{
	if (pc->sz == pc->capacity)
	{
		printf("容量已满，即将开始增容\n");
		peoino* ptr=(peoino*)realloc(pc->data, 2 * sizeof(peoino));
		if (ptr != NULL)
		{
			pc->data = ptr;
			pc->capacity += inc_sz;
			printf("增容成功\n");
		}
		else
		{
			perror("Addcontact");
			printf("增容失败，增加联系人错误\n");
			return;
		}
	}
	printf("请输入联系人姓名：");
	scanf("%s", pc->data[pc->sz].name);
	printf("请输入联系人性别：");
	scanf("%s", pc->data[pc->sz].sex);
	printf("请输入联系人年龄：");
	scanf("%d", &(pc->data[pc->sz].age));
	printf("请输入联系人号码：");
	scanf("%s", pc->data[pc->sz].tele);
	printf("请输入联系人地址：");
	scanf("%s", pc->data[pc->sz].addr);
	printf("添加成功\n");
	pc->sz++;
}
//打印电话簿
void Printcontact(const Contact* pc)
{

	printf("%-10s\t%-5s\t%-5s\t%-12s\t%-20s\n", "姓名", "性别", "年龄", "号码", "地址");
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		printf("%-10s\t%-5s\t%-5d\t%-12s\t%-20s\n", pc->data[i].name,
			pc->data[i].sex,
			pc->data[i].age,
			pc->data[i].tele,
			pc->data[i].addr);
	}
	printf("打印成功\n");
}

//通过姓名查找
int fandbyname(Contact* pc, char* target)
{
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		if (strcmp((pc->data[i].name), target) == 0)
		{
			return i;
		}
	}
	return -1;
}
//删除联系人
void Delcontact(Contact* pc)
{
	if (pc->sz == 0)
	{
		printf("通讯录中没有联系人！\n");
		return;
	}
	char name[MAX_name] = { 0 };
	printf("请输入要删除联系人的姓名：");
	scanf("%s", name);
	int pos = fandbyname(pc, name);
	if (pos == -1)
	{
		printf("通讯录中查无此人！\n");
		return;
	}
	int i = 0;
	for (i = pos; i < pc->sz - 1; i++)
	{
		pc->data[i] = pc->data[i + 1]; 
	}
	printf("删除成功\n");
	pc->sz--;
}
//查找联系人
void Searchcontact(const Contact* pc)
{
	if (pc->sz == 0)
	{
		printf("通讯录中没有联系人！\n");
		return;
	}
	char name[MAX_name] = { 0 };
	printf("请输入要查找联系人的姓名：");
	scanf("%s", name);
	int pos = fandbyname(pc, name);
	if (pos == -1)
	{
		printf("通讯录中查无此人！\n");
		return;
	}
	printf("该联系人信息如下：\n");
	printf("%-10s\t%-5s\t%-5s\t%-12s\t%-20s\n", "姓名", "性别", "年龄", "号码", "地址");
	printf("%-10s\t%-5s\t%-5d\t%-12s\t%-20s\n", pc->data[pos].name,
		pc->data[pos].sex,
		pc->data[pos].age,
		pc->data[pos].tele,
		pc->data[pos].addr);
}
//修改联系人
void Modifycontact(Contact* pc)
{
	if (pc->sz == 0)
	{
		printf("通讯录中没有联系人！\n");
		return;
	}
	char name[MAX_name] = { 0 };
	printf("请输入要修改联系人的姓名：");
	scanf("%s", name);
	int pos = fandbyname(pc, name);
	if (pos == -1)
	{
		printf("通讯录中查无此人！\n");
		return;
	}
	printf("该联系人信息如下：\n");
	printf("%-10s\t%-5s\t%-5s\t%-12s\t%-20s\n", "姓名", "性别", "年龄", "号码", "地址");
	printf("%-10s\t%-5s\t%-5d\t%-12s\t%-20s\n", pc->data[pos].name,
		pc->data[pos].sex,
		pc->data[pos].age,
		pc->data[pos].tele,
		pc->data[pos].addr);
	printf("请输入修改后联系人姓名：");
	scanf("%s", pc->data[pos].name);
	printf("请输入修改后联系人性别：");
	scanf("%s", pc->data[pos].sex);
	printf("请输入修改后联系人年龄：");
	scanf("%d", &(pc->data[pos].age));
	printf("请输入修改后联系人号码：");
	scanf("%s", pc->data[pos].tele);
	printf("请输入修改后联系人地址：");
	scanf("%s", pc->data[pos].addr);
	printf("修改成功\n");
}

//按名字排序
int sort(const void* e1, const void* e2)
{
	return (((peoino*)e1)->name, ((peoino*)e2)->name);
}
void sort_name(Contact* pc)
{
	qsort(pc->data, pc->sz, sizeof(pc->data[0]), sort);
}


void savecontact(Contact* pc)
{
	//打开文件
	FILE* pf = fopen("contact.dat", "w");
	if (NULL == pf)
	{
		perror("fopen");
		return;
	}
	//存入数据
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		fwrite(pc->data + i, sizeof(peoino), 1, pf);
	}
	//关闭文件
	fclose(pf);
	pf = NULL;
}

void checkcapacity(Contact* pc)
{
	if (pc->sz == pc->capacity)
	{
		printf("容量已满，即将开始增容\n");
		peoino* ptr = (peoino*)realloc(pc->data, 2 * sizeof(peoino));
		if (ptr != NULL)
		{
			pc->data = ptr;
			pc->capacity += inc_sz;
			printf("增容成功\n");
		}
		else
		{
			perror("Addcontact");
			printf("增容失败\n");
			return;
		}
	}
}
