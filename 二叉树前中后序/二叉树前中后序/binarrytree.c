#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>

//节点的度: -个节点含有的子树的个数称为该节点的度; 如上图: A的为6
//叶节点或终端节点 : 度为0的节点称为叶节点:如上图: B.C.H. ..等节点为叶节点
//非终端节点或分支节点 : 度不为0的节点; 如上图: D.E.F. ...等节点为分支节点
//双亲节点或父节点 : 若 - 个节点含有子节点，则这个节点称为其子节点的父节点; 如上图: A是BE
//孩子节点或子节点 : 一个节点含有的子树的根节点称为该节点的子节点; 如上图: B是A的孩子节
//兄弟节点 : 具有相同父节点的节点互称为兄弟节点; 如上图: B.C是兄弟节点
//树的度 : 一棵树中，最大的节点的度称为树的度; 如上图: 树的度为6
//节点的层次 : 从根开始定义起，根为第1层，根的子节点为第2层，以此类推;
//树的高度或深度:树中节点的最大层次; 如 上图 : 树的高度为4
//节点的祖先 : 从根到该节点所经分支上的所有节点; 如上图: A是所有节点的祖先
//子孙 : 以某节点为根的子树中任一节点都称为该节点的子孙。 如上图 : 所有节点都是A的子孙
//森林 : 由m(m > 0) 棵互不相交的多颗树的集合称为森林; (数据结构中的学 习并查集本质就是一//


typedef char BTData;
typedef struct BinarryTreeNode
{
	BTData data;
	struct BinarryTreeNode* left;
	struct BinarryTreeNode* right;

}BTNode;


//前序二叉树
void PrevOrder(BTNode* root)
{
	if (!root)
	{
		printf("NULL ");
		return;
	}
	printf("%c ", root->data);
	PrevOrder(root->left);
	PrevOrder(root->right);
}

//中序二叉树
void InOrder(BTNode* root)
{
	if (!root)
	{
		printf("NULL ");
		return;
	}
	InOrder(root->left);
	printf("%c ", root->data);
	InOrder(root->right);
}

//后序二叉树
void PostOrder(BTNode* root)
{
	if (!root)
	{
		printf("NULL ");
		return;
	}
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%c ", root->data);
}

//求结点个数
//void TreeNode(BTNode* root, int* size)
//{
//	if (!root)
//	{
//		return 0;
//	}
//	++(*size);
//	TreeNode(root->left, size);
//	TreeNode(root->right, size);
//}

//求节点个数
int TREENode(BTNode* root)
{
	return root==NULL? 0:TREENode(root->left) + TREENode(root->right) + 1;
}

//求叶节点个数
int LeafNode(BTNode* root)
{
	if (!root)
	{
		return 0;
	}
	return !root->left && !root->right ? 1 : LeafNode(root->left) + LeafNode(root->right);
}

int main()
{
	BTNode* E = (BTNode*)malloc(sizeof(BTNode));
	if (!E)
	{
		perror("malloc");
		return;
	}
	E->data = 'E';
	E->left = NULL;
	E->right = NULL;

	BTNode* D = (BTNode*)malloc(sizeof(BTNode));
	D->data = 'D';
	D->left = NULL;
	D->right = NULL;

	BTNode* F = (BTNode*)malloc(sizeof(BTNode));
	F->data = 'F';
	F->left = NULL;
	F->right = NULL;

	BTNode* C = (BTNode*)malloc(sizeof(BTNode));
	C->data = 'C';
	C->left = F;
	C->right = NULL;

	BTNode* B = (BTNode*)malloc(sizeof(BTNode));
	B->data = 'B';
	B->left = D;
	B->right = E;

	BTNode* A = (BTNode*)malloc(sizeof(BTNode));
	A->data = 'A';
	A->left = B;
	A->right = C;	
	
	//printf("前序二叉树：");
	//PrevOrder(A);
	//printf("\n");	
	//
	//printf("中序二叉树：");
	//InOrder(A);
	//printf("\n");	
	//
	//printf("后序二叉树：");
	//PostOrder(A);
	//printf("\n");


	////计算节点个数
	//int Asize = 0;
	//TreeNode(A, &Asize);
	//printf("A树节点个数为：%d\n", Asize);
	//int Bsize = 0;
	//TreeNode(B, &Bsize);
	//printf("B树节点个数为：%d\n", Bsize);

	//计算节点个数
	//printf("A树的节点是: %d\n", TREENode(A));
	//printf("B树的节点是: %d\n", TREENode(B));

	//计算叶节点个数
	printf("A树的叶节点数为：%d\n", LeafNode(A));
	printf("B树的叶节点数为：%d\n", LeafNode(B));
	printf("F树的叶节点数为：%d\n", LeafNode(F));

	return 0;
}