#include "List.h"

void listtest1()
{
	LNode* phead = listinit();
	ListPushBack(phead, 3);
	ListPushBack(phead, 2);
	ListPushBack(phead, 3);
	ListPushBack(phead, 4);
	ListPushBack(phead, 9);
	ListPrint(phead);
	ListPushFront(phead, 10);
	ListPrint(phead);
	ListPopFront(phead);
	ListPrint(phead);
	//ListDestroy();
}
void list()
{
	LNode* phead = listinit();
	ListPushBack(phead, 3);
	ListPushBack(phead, 2);
	ListPushBack(phead, 3);
	ListPushBack(phead, 4);
	ListPushBack(phead, 9);
	ListPrint(phead);

	ListPushFront(phead, 9847);
	ListPrint(phead);

	ListPopBack(phead);
	ListPrint(phead);

	LNode* pos = ListFindByData(phead, 4);
	ListInsert(pos, 999);
	ListPrint(phead);
	ListErase(pos);
	ListPrint(phead);
}

int main()
{
	int n = 5;
	//listtest1();
	list();
	return 0;
}