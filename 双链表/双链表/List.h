#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int LDataType;

typedef struct ListNode
{
	LDataType data;
	struct ListNode* prev;
	struct ListNode* next;
}LNode;

//初始化双链表
LNode* listinit();

//创建一个节点
LNode* BuyListNode(LDataType x);

//尾插
void ListPushBack(LNode* phead, LDataType x);

//打印链表
void ListPrint(LNode* phead);

//尾删
void ListPopBack(LNode* phead);

//头插
void ListPushFront(LNode* phead, LDataType x);

//头删
void ListPopFront(LNode* phead);

//通过值查找
LNode* ListFindByData(LNode* phead, LDataType x);

//pos位置之前插入值
void ListInsert(LNode* pos, LDataType x);

//删除pos位置
void ListErase(LNode* pos);

//销毁链表
void ListDestroy(LNode* phead);