#include "List.h"

LNode* listinit()
{
	LNode* phead = (LNode*)malloc(sizeof(LNode));
	if (phead == NULL)
	{
		printf("初始化失败\n");
		exit(-1);
	}
	phead->data = 0;
	phead->prev = phead;
	phead->next = phead;
	return phead;
}

LNode* BuyListNode(LDataType x)
{
	LNode* newnode = (LNode*)malloc(sizeof(LNode));
	if (newnode == NULL)
	{
		printf("创建新节点失败\n");
		exit(-1);
	}
	newnode->data = x;
	newnode->prev = NULL;
	newnode->next = NULL;
	return newnode;
}

void ListPushBack(LNode* phead,LDataType x)
{
	//LNode* new = BuyListNode(x);
	//LNode* tail = phead->prev;
	//tail->next = new;
	//phead->prev = new;
	//new->prev = tail;
	//new->next = phead;

	assert(phead);
	ListInsert(phead->prev,x);
}

void ListPrint(LNode* phead)
{
	LNode* ppp = phead->next;
	while (ppp != phead)
	{
		printf("%d ", ppp->data);
		ppp = ppp->next;
	}
	printf("\n");
}

void ListPopBack(LNode* phead)
{
	//LNode* tail = phead->prev;
	//LNode* ptail = tail->prev;
	//free(tail);
	//ptail->next = phead;
	//phead->prev = ptail;
	assert(phead);
	assert(phead->prev != phead);
	ListErase(phead->prev);
}

void ListPushFront(LNode* phead, LDataType x)
{

	//LNode* new = BuyListNode(x);
	//LNode* first = phead->next;
	//new->prev = phead;
	//new->next = first;
	//phead->next = new;
	//first->prev = new;
	assert(phead);
	ListInsert(phead->next,x);
}

void ListPopFront(LNode* phead)
{
	//if (phead->next == phead)
	//{
	//	printf("该链表为空\n");
	//	exit(-1);
	//}
	//LNode* first = phead->next;
	//LNode* newfirst = first->next;
	//free(first);
	//phead->next = newfirst;
	//newfirst->prev = phead;
	assert(phead);
	assert(phead->next);

	ListErase(phead->next);

}

LNode* ListFindByData(LNode* phead, LDataType x)
{
	assert(phead);
	LNode* cur = phead->next;
	while (cur!=phead)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

void ListInsert(LNode* pos, LDataType x)
{
	assert(pos);
	LNode* prev = pos->prev;

	LNode* newnode = BuyListNode(x);

	prev->next = newnode;
	newnode->next = pos;
	newnode->prev = prev;
	pos->prev = newnode;
}

void ListErase(LNode* pos)
{
	LNode* prev = pos->prev;
	LNode* after = pos->next;
	prev->next = after;
	after->prev = prev;
	free(pos);
	pos = NULL;
}

void ListDestroy(LNode* phead)
{
	assert(phead);
	LNode* cur = phead->next;
	while (cur != phead)
	{
		LNode* next = cur->next;
		free(cur);
		cur = NULL;
		cur = next;
	}
	free(phead);
}