#include "Queue.h"

void test1()
{
	Queue q;
	QueueInit(&q);
	QueuePush(&q, 1);
	QueuePush(&q, 3);
	QueuePush(&q, 5);
	QueuePush(&q, 9);
	QueuePrint(&q);

	QueuePop(&q);
	QueuePrint(&q);

	QueuePop(&q);
	QueuePrint(&q);

	printf("%d \n", QueueFront(&q));
	printf("%d \n", QueueBack(&q));

}



int main()
{
	test1();
	return 0;
}