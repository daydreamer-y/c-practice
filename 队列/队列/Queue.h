#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
typedef int DataType;
typedef struct QueueNode
{
	struct QueueNode* next;
	DataType data;
}QNode;

typedef struct Queue
{
	QNode* head;
	QNode* tail;
	int size;
}Queue;


//初始化队列
void QueueInit(Queue* pq);

//对尾入
void QueuePush(Queue* pq, DataType x);

//打印队列
void QueuePrint(Queue* pq);

//对头出
void QueuePop(Queue* pq);

//销毁队列
void QueueDestroy(Queue* pq);

//取对头数据
DataType QueueFront(Queue* pq);

//取队尾数据
DataType QueueBack(Queue* pq);

//判断是否为空
bool QueueEmpty(Queue* pq);

//元素个数
int QueueSize(Queue* pq);