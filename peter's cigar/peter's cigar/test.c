#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//Peter有n根烟, 他每吸完 - 根烟就把烟蒂保存起来， k(k > 1) 个烟蒂可以换一个新的烟，那么Peter
//最终能吸到多少根烟呢 ?

int main()
{
    int n = 0;
    int k = 0;
    scanf("%d %d", &n, &k);
    int sum = n;
    while (n >= k)
    {
        sum += n / k;
        n = n % k + n / k;
    }
    printf("%d\n", sum);
    return 0;
}